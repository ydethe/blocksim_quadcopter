==============================================================================
blocksim_quadcopter
==============================================================================


.. image:: https://img.shields.io/pypi/v/blocksim_quadcopter.svg
        :target: https://pypi.python.org/pypi/blocksim_quadcopter

.. image:: https://readthedocs.org/projects/blocksim_quadcopter/badge/?version=latest
        :target: https://blocksim_quadcopter.readthedocs.io/en/latest/?badge=latest
        :alt: Documentation Status

.. image:: https://gitlab.com/ydethe/blocksim_quadcopter/badges/master/pipeline.svg
   :target: https://gitlab.com/ydethe/blocksim_quadcopter/pipelines

.. image:: https://codecov.io/gl/ydethe/blocksim_quadcopter/branch/master/graph/badge.svg
  :target: https://codecov.io/gl/ydethe/blocksim_quadcopter

.. image:: https://img.shields.io/pypi/dm/blocksim_quadcopter
  :target: https://pypi.python.org/pypi/blocksim_quadcopter

A set of computers to simulate a quadcopter

Free software: MIT license

Development
-----------

In your virtual env::

    python setup.py develop

Features
--------

* TODO
